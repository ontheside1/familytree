import { useEffect, useState } from "react";
import "./App.css";
import { FamilyTree } from "./FamilyTree";
import { Info } from "./Info";

function App() {
    const [details, setDetails] = useState({ name: "", role: "", hierarchy: "" });
    const [edit, setEdit] = useState(false);
    const [family, setFamily] = useState({
        grandfather1name: "",
        grandmother1name: "",
        grandfather1child: "",
        grandfather2name: "",
        grandmother2name: "",
        grandfather2child: "",
        fathername: "",
        hisfathername: "",
        hismothername: "",
        mothername: "",
        herfathername: "",
        hermothername: "",
        children: "",
    });

    async function getFamily() {
        const response = await fetch("http://localhost:4000");
        const data = await response.json();
        console.log(data);
        if (data[0]) {
            setFamily(data[0]);
        }
    }

    useEffect(() => {
        getFamily();
    }, []);
    return (
        <main className="container-fluid py-4">
            <section className="mx-auto w-50 text-center">
                <button
                    className="btn btn-success"
                    onClick={async () => {
                        await getFamily();
                        setEdit(!edit);
                    }}
                >
                    {edit ? "Back to Family Tree" : "Edit Family Tree"}
                </button>
            </section>
            {edit ? (
                <FamilyTree family={family} set={setEdit} />
            ) : (
                <section className="tree text-center">
                    {/*Grand Parents*/}
                    <div className="grandParents d-flex justify-content-center my-5">
                        {family.grandfather1name && (
                            <button
                                className="alert alert-success rounded mx-3"
                                data-bs-toggle="modal"
                                data-bs-target="#exampleModal"
                                onClick={() =>
                                    setDetails({
                                        name: family.grandfather1name,
                                        role: "Grand Father",
                                        hierarchy: "Grand Parent (Father's side)",
                                    })
                                }
                            >
                                {family.grandfather1name}
                            </button>
                        )}
                        {family.grandmother1name && (
                            <button
                                className="alert alert-success rounded mx-3"
                                data-bs-toggle="modal"
                                data-bs-target="#exampleModal"
                                onClick={() =>
                                    setDetails({
                                        name: family.grandmother1name,
                                        role: "Grand Mother",
                                        hierarchy: "Grand Parent (Father's side)",
                                    })
                                }
                            >
                                {family.grandmother1name}
                            </button>
                        )}
                        {family.grandfather2name && (
                            <button
                                className="alert alert-primary rounded mx-3"
                                data-bs-toggle="modal"
                                data-bs-target="#exampleModal"
                                onClick={() =>
                                    setDetails({
                                        name: family.grandfather2name,
                                        role: "Grand Father",
                                        hierarchy: "Grand Parent (Mother's side)",
                                    })
                                }
                            >
                                {family.grandfather2name}
                            </button>
                        )}
                        {family.grandmother2name && (
                            <button
                                className="alert alert-primary rounded mx-3"
                                data-bs-toggle="modal"
                                data-bs-target="#exampleModal"
                                onClick={() =>
                                    setDetails({
                                        name: family.grandmother2name,
                                        role: "Grand Mother",
                                        hierarchy: "Grand Parent (Mother's side)",
                                    })
                                }
                            >
                                {family.grandmother2name}
                            </button>
                        )}
                    </div>
                    {/*Parents*/}
                    <div className="parents d-flex justify-content-center my-5">
                        {family.fathername && (
                            <button
                                className="alert alert-success rounded mx-3"
                                data-bs-toggle="modal"
                                data-bs-target="#exampleModal"
                                onClick={() =>
                                    setDetails({
                                        name: family.fathername,
                                        role: "Father",
                                        hierarchy: "Parent",
                                    })
                                }
                            >
                                {family.fathername}
                            </button>
                        )}
                        {family.mothername && (
                            <button
                                className="alert alert-primary rounded mx-3"
                                data-bs-toggle="modal"
                                data-bs-target="#exampleModal"
                                onClick={() =>
                                    setDetails({
                                        name: family.mothername,
                                        role: "Mother",
                                        hierarchy: "Parent",
                                    })
                                }
                            >
                                {family.mothername}
                            </button>
                        )}
                    </div>
                    <div className="children d-flex justify-content-center">
                        {family.children &&
                            family.children.split(",").map((child, i) => (
                                <button
                                    key={i}
                                    className="alert alert-warning rounded mx-3"
                                    data-bs-toggle="modal"
                                    data-bs-target="#exampleModal"
                                    onClick={() =>
                                        setDetails({
                                            name: child,
                                            role: "Sibling",
                                            hierarchy: "Children",
                                        })
                                    }
                                >
                                    {child.trim()}
                                </button>
                            ))}
                    </div>
                </section>
            )}
            <Info details={details} />
        </main>
    );
}

export default App;
