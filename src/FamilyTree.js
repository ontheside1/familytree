import { useEffect, useState } from "react";

export function FamilyTree(props) {
    const { family } = props;

    const [children, setChildren] = useState(family.children);
    const [gf1name, set_gf1name] = useState(family.grandfather1name);
    const [gf1c, set_gf1c] = useState(family.grandfather1child);
    const [gf2name, set_gf2name] = useState(family.grandfather2name);
    const [gf2c, set_gf2c] = useState(family.grandfather2child);
    const [gm1name, set_gm1name] = useState(family.grandmother1name);
    const [gm2name, set_gm2name] = useState(family.grandmother2name);
    const [fname, set_fname] = useState(family.fathername);
    const [mname, set_mname] = useState(family.mothername);

    async function save() {
        const newFamily = {
            grandfather1name: gf1name,
            grandmother1name: gm1name,
            grandfather2name: gf2name,
            grandmother2name: gm2name,
            fathername: fname,
            mothername: mname,
            children: children,
        };

        await fetch("http://localhost:4000/", {
            method: "DELETE", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, *cors, same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
            },
        });

        const response = await fetch("http://localhost:4000/", {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, *cors, same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(newFamily),
        });
        console.log(await response.json());
        alert("Done saving");
    }

    useEffect(() => {}, []);
    return (
        <main className="container my-4 w-75 text-center">
            <section className="alert alert-success shadow my-4">
                <h3 className="mb-3">Grand Parent (Father side)</h3>
                <div className="input-group mb-3">
                    <span className="input-group-text px-5" id="basic-addon1">
                        Grad Father's Name
                    </span>
                    <input
                        type="text"
                        className="form-control"
                        placeholder="First Name, Last Name"
                        value={gf1name}
                        onChange={(e) => {
                            set_gf1name(e.target.value);
                        }}
                    />
                </div>

                <div className="input-group mb-3">
                    <span className="input-group-text px-5" id="basic-addon1">
                        Grad Mother's Name
                    </span>
                    <input
                        type="text"
                        className="form-control"
                        placeholder="First Name, Last Name"
                        value={gm1name}
                        onChange={(e) => {
                            set_gm1name(e.target.value);
                        }}
                    />
                </div>
            </section>

            <section className="alert alert-primary shadow my-4">
                <h3 className="mb-3">Grand Parent (Mother side)</h3>
                <div className="input-group mb-3">
                    <span className="input-group-text px-5" id="basic-addon1">
                        Grad Father's Name
                    </span>
                    <input
                        type="text"
                        className="form-control"
                        placeholder="First Name, Last Name"
                        value={gf2name}
                        onChange={(e) => {
                            set_gf2name(e.target.value);
                        }}
                    />
                </div>

                <div className="input-group mb-3">
                    <span className="input-group-text px-5" id="basic-addon1">
                        Grad Mother's Name
                    </span>
                    <input
                        type="text"
                        className="form-control"
                        placeholder="First Name, Last Name"
                        value={gm2name}
                        onChange={(e) => {
                            set_gm2name(e.target.value);
                        }}
                    />
                </div>
            </section>

            <section className="alert alert-info shadow my-4">
                <h3 className="mb-3">Parents</h3>
                <div className="input-group mb-3">
                    <span className="input-group-text px-5 text-success" id="basic-addon1">
                        Father's Name
                    </span>
                    <input
                        type="text"
                        className="form-control"
                        placeholder="First Name, Last Name"
                        value={fname}
                        onChange={(e) => {
                            set_fname(e.target.value);
                        }}
                    />
                </div>

                <div className="input-group mb-3">
                    <span className="input-group-text px-5 text-primary" id="basic-addon1">
                        Mother's Name
                    </span>
                    <input
                        type="text"
                        className="form-control"
                        placeholder="First Name, Last Name"
                        value={mname}
                        onChange={(e) => {
                            set_mname(e.target.value);
                        }}
                    />
                </div>
            </section>

            <section className="alert alert-warning shadow my-4">
                <h3 className="mb-3">Children</h3>
                <div className="input-group mb-3">
                    <span className="input-group-text px-5" id="basic-addon1">
                        Names:
                    </span>
                    <input
                        type="text"
                        className="form-control text-center"
                        placeholder="Names separated by comma"
                        value={children}
                        onChange={(e) => setChildren(e.target.value)}
                    />
                </div>
            </section>
            <div className="sticky-bottom">
                <button className="btn btn-light px-5" onClick={save}>
                    SAVE CHANGES
                </button>
            </div>
        </main>
    );
}
