const express = require("express");
const app = express();
const mongoose = require("mongoose");
const Family = require("./family");
const cors = require("cors");

app.use(cors());

/**
 * To edit the mongodb server, kindly create your own account in MongoDB atlas
 * just change the credentials of the "mongoose.connect"
 * this tutorial might help:
 * https://www.youtube.com/watch?v=sx2zTNiaBuM
 */
const USER = "admin";
const PASSWORD = "admin";

mongoose.connect(`mongodb+srv://${USER}:${PASSWORD}@cluster0.i9vlq.mongodb.net/family`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
mongoose.connection.once("open", () => console.log("Connected to MongoDB atlas"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", async (req, res) => {
    const result = await Family.find();
    res.send(result);
});

app.post("/", async (req, res) => {
    const newFamily = new Family(req.body);
    await newFamily.save();
    res.send(req.body);
});

app.delete("/", async (req, res) => {
    await Family.findOneAndDelete({});
    res.send({ status: "existing family removed" });
});

app.listen(4000, () => console.log("Server running on PORT", 4000));
