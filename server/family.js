const mongoose = require("mongoose");

const familySchema = new mongoose.Schema({
    grandfather1name: String,
    grandmother1name: String,
    grandfather2name: String,
    grandmother2name: String,
    fathername: String,
    mothername: String,
    children: String,
});

module.exports = mongoose.model("Family", familySchema);
